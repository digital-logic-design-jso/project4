module DPP4(input [7:0] a, b, input s, input clk, input [2:0] f, shif, output logic [7:0]w);
  //P4
  wire [7:0] j;
  logic [7:0] k;
  REG8 R1(k, clk, w);
  MUX8 M1(b, w, s, j); 
  ALU1 A1(a, j, f, shif, k); 
endmodule

module ALU1 (input signed [7:0] A, B , input [2:0] f, shif, output logic [7:0] w); 
  always @(A,B,f,shif) begin
    w = 8'b0;
    case(f)
      0 : w = A + B;
      1 : w = A + B;
      2 : if( A > B )
            w = A;
        else w = B;
      3 : w =  A + (A <<< shif);
      4 : w =  A + (A >>> shif);
      5 : if ( A[7] == 1 )
            w = ~A + 8'b00000001;
        else w = A;
      6 : w = A + (B << 1);
      7 : w = A & B;
      default : w = 8'bxxxxxxxx;
    endcase
  end
endmodule

module MUX8(input [7:0] a, b, input s, output [7:0] w);
  assign w = s ? b : a;
endmodule

module REG8(input [7:0] d, input clk, output [7:0] w);
  DLP2 d0(d[0], clk, w[0]);
  DLP2 d1(d[1], clk, w[1]);
  DLP2 d2(d[2], clk, w[2]);
  DLP2 d3(d[3], clk, w[3]);
  DLP2 d4(d[4], clk, w[4]);
  DLP2 d5(d[5], clk, w[5]);
  DLP2 d6(d[6], clk, w[6]);
  DLP2 d7(d[7], clk, w[7]);
endmodule