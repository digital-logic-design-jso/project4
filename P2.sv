module DLP2 (input d, clk, output Q);
  //P2
  wire db;
  //wire j, k, Qb;
  not #(6,4)T1(db,d);
  //nand #(6,8)n1(j, d, clk);
  //nand #(6,8)n2(k, db, clk);
  //nand #(6,8)n3(Q, j, Qb);
  //nand #(6,8)n4(Qb, k, Q);
  SRLP1 p1(d, db, clk, Q);
endmodule
