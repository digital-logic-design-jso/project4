module DFFP7(input d, clk, output Q);
  //P7
  wire j;
  DLP2 d0(d, clk, j);
  DLP2 d1(j, ~clk, Q);
endmodule
