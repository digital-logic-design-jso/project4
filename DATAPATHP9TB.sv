module DPP9TB();
  //P10
  logic [7:0 ] ww;
  reg [7:0] aa, bb;
  reg [2:0] ff,sh;
  reg cc, ss;
  DPP9 d1(aa, bb, ss, cc, ff, sh, ww);
  initial begin
    ss = 0;
    ff = 3'b000;
    aa = 8'b00001000;
    bb = 8'b00000001;
    cc = 0;
    #300;
    cc = 1;
    #300;
    cc = 0;
    #300;
    ss = 1;
    #300
    cc = 1;
    #300;
    cc = 0;
    #300;
    cc = 1;
    #300;
    ss = 0;
    #300;
    cc = 0;
    #300;
    cc = 1;
    #300;
    ss = 1;
    #300;
    cc = 0;
    #300;
    ss = 1;
    cc = 1;
    #300;
    ff = 3'b001;
    #300;
    $stop;
  end
endmodule
