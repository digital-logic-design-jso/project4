module SRLP1TB();
  //P1
  wire QQ;
  reg ss, rr, cc;
  SRLP1 s1(ss, rr, cc, QQ);
  initial begin
    ss = 0;
    rr = 0;
    cc = 0;
    #40
    ss = 1;
    rr = 0;
    cc = 1;
    #40
    ss = 0;
    rr = 1;
    cc = 1;
    #40
    ss = 1;
    rr = 0;
    cc = 1;
    #40
    ss = 1;
    rr = 1;
    cc = 1;
    #40
    ss = 0;
    rr = 1;
    cc = 1;
    #40
    ss = 1;
    rr = 0;
    cc = 1;
    #40
    ss = 1;
    rr = 1;
    cc = 1;
    #40
    ss = 0;
    rr = 1;
    cc = 1;
    #40
    ss = 1;
    rr = 1;
    cc = 1;
    #40
    ss = 1;
    rr = 1;
    cc = 0;
    #40
    $stop;
  end
  
endmodule
