module DPP9(input [7:0] a, b, input s, input clk, input [2:0] f, shif, output logic [7:0]w);
  //P9
  wire [7:0] j;
  logic [7:0] k;
  REG82 R1(k, clk, w);
  MUX82 M1(b, w, s, j); 
  ALU2 A1(a, j, f, shif, k); 
endmodule

module ALU2 (input signed [7:0] A, B , input [2:0] f, shif, output logic [7:0] w); 
  always @(A,B,f,shif) begin
    w = 8'b0;
    case(f)
      0 : w = A + B;
      1 : w = A + B;
      2 : if( A > B )
            w = A;
        else w = B;
      3 : w =  A + (A <<< shif);
      4 : w =  A + (A >>> shif);
      5 : if ( A[7] == 1 )
            w = ~A + 8'b00000001;
        else w = A;
      6 : w = A + (B << 1);
      7 : w = A & B;
      default : w = 8'bxxxxxxxx;
    endcase
  end
endmodule

module MUX82(input [7:0] a, b, input s, output [7:0] w);
  assign w = s ? b : a;
endmodule

module REG82(input [7:0] d, input clk, output [7:0] w);
  DFFP7 d0(d[0], clk, w[0]);
  DFFP7 d1(d[1], clk, w[1]);
  DFFP7 d2(d[2], clk, w[2]);
  DFFP7 d3(d[3], clk, w[3]);
  DFFP7 d4(d[4], clk, w[4]);
  DFFP7 d5(d[5], clk, w[5]);
  DFFP7 d6(d[6], clk, w[6]);
  DFFP7 d7(d[7], clk, w[7]);
endmodule

