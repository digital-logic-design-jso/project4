module DFFP7TB();
  //P8
  logic QQ;
  reg dd, cc;
  DFFP7 d1(dd, cc, QQ);
  initial begin
    dd = 0;
    cc = 0;
    #40
    dd = 0;
    cc = 1;
    #40
    dd = 0;
    cc = 0;
    #40
    dd = 1;
    cc = 1;
    #40
    dd = 1;
    cc = 0;
    #40
    dd = 0;
    cc = 1;
    #40
    dd = 1;
    cc = 0;
    #40
    $stop;
  end
endmodule
