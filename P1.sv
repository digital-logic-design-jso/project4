module SRLP1(input s, r, clk, output Q);
  //P1
  wire j, k, Qb;
  nand #(6,8)n1(j, s, clk);
  nand #(6,8)n2(k, r, clk);
  nand #(6,8)n3(Q, j, Qb);
  nand #(6,8)n4(Qb, k, Q);
endmodule
