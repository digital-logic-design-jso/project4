module DLP2TB();
  //P3
  wire QQ;
  reg dd, cc;
  DLP2 d1(dd, cc, QQ);
  initial begin
    dd = 0;
    cc = 0;
    #40
    dd = 0;
    cc = 1;
    #40
    dd = 1;
    cc = 1;
    #40
    dd = 0;
    cc = 1;
    #40
    $stop;
  end
endmodule
